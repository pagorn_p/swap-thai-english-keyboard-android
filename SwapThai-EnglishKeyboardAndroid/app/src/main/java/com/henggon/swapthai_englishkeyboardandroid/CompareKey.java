package com.henggon.swapthai_englishkeyboardandroid;

/**
 * Created by Pagorn on 3/11/2558.
 */
public class CompareKey {
    public static char compareKeyThaiToEng(char c)
    {
        switch (c)
        {
            case 'ๅ': return '1';
            case '+': return '!';
            case '/': return '2';
            case '๑': return '@';
            case '-': return '3';
            case '๒': return '#';
            case '4': return 'ภ';
            case '$': return '๓';
            case '5': return 'ถ';
            case '%': return '๔';
            case 'ุ': return '6';
            case 'ู': return '^';
            case 'ึ': return '7';
            case '฿': return '&';
            case 'ค': return '8';
            case '๕': return '*';
            case 'ต': return '9';
            case '๖': return '('; //
            case 'จ': return '0';
            case '๗': return ')';
            case 'ข': return '-';
            case '๘': return '_';
            case 'ช': return '=';
            case '๙': return '+';
            case 'ฃ': return '\\';
            case 'ฅ': return '|';

            case 'ๆ': return 'q';
            case '๐': return 'Q';
            case 'ไ': return 'w';
            case '\"': return 'W';
            case 'ำ': return 'e';
            case 'ฎ': return 'E';
            case 'พ': return 'r';
            case 'ฑ': return 'R';
            case 'ะ': return 't';
            case 'ธ': return 'T';
            case 'ั': return 'y';
            case 'ํ': return 'Y';
            case 'ี': return 'u';
            case '๊': return 'U';
            case 'ร': return 'i';
            case 'ณ': return 'I';
            case 'น': return 'o';
            case 'ฯ': return 'O';
            case 'ย': return 'p';
            case 'ญ': return 'P';
            case 'บ': return '[';
            case 'ฐ': return '{';
            case 'ล': return ']';
            case ',': return '}';


            case 'ฟ': return 'a';
            case 'ฤ': return 'A';
            case 'ห': return 's';
            case 'ฆ': return 'S';
            case 'ก': return 'd';
            case 'ฏ': return 'D';
            case 'ด': return 'f';
            case 'โ': return 'F';
            case 'เ': return 'g';
            case 'ฌ': return 'G';
            case '้': return 'h';
            case '็': return 'H';
            case '่': return 'j';
            case '๋': return 'J';
            case 'า': return 'k';
            case 'ษ': return 'K';
            case 'ส': return 'l';
            case 'ศ': return 'L';
            case 'ว': return ';';
            case 'ซ': return ':';
            case 'ง': return '\'';
            case '.': return '\"';

            case 'ผ': return 'z';
            case '(': return 'Z';
            case 'ป': return 'x';
            case ')': return 'X';
            case 'แ': return 'c';
            case 'ฉ': return 'C';
            case 'อ': return 'v';
            case 'ฮ': return 'V';
            case 'ิ': return 'b';
            case 'ฺ': return 'B';
            case 'ื': return 'n';
            case '์': return 'N';
            case 'ท': return 'm';
            case '?': return 'M';
            case 'ม': return ',';
            case 'ฒ': return '<';
            case 'ใ': return '.';
            case 'ฬ': return '>';
            case 'ฝ': return '/';
            case 'ฦ': return '?';
            default:
                return c;
        }
    }

    public static char compareKeyEngToThai(char c)
    {
        switch (c)
        {
            case '1': return 'ๅ';
            case '!': return '+';
            case '2': return '/';
            case '@': return '๑';
            case '3': return '-';
            case '#': return '๒';
            case '4': return 'ภ';
            case '$': return '๓';
            case '5': return 'ถ';
            case '%': return '๔';
            case '6': return 'ุ';
            case '^': return 'ู';
            case '7': return 'ึ';
            case '&': return '฿';
            case '8': return 'ค';
            case '*': return '๕';
            case '9': return 'ต';
            case '(': return '๖';
            case '0': return 'จ';
            case ')': return '๗';
            case '-': return 'ข';
            case '_': return '๘';
            case '=': return 'ช';
            case '+': return '๙';
            case '\\': return 'ฃ';
            case '|': return 'ฅ';

            case 'q': return 'ๆ';
            case 'Q': return '๐';
            case 'w': return 'ไ';
            case 'W': return '\"';
            case 'e': return 'ำ';
            case 'E': return 'ฎ';
            case 'r': return 'พ';
            case 'R': return 'ฑ';
            case 't': return 'ะ';
            case 'T': return 'ธ';
            case 'y': return 'ั';
            case 'Y': return 'ํ';
            case 'u': return 'ี';
            case 'U': return '๊';
            case 'i': return 'ร';
            case 'I': return 'ณ';
            case 'o': return 'น';
            case 'O': return 'ฯ';
            case 'p': return 'ย';
            case 'P': return 'ญ';
            case '[': return 'บ';
            case '{': return 'ฐ';
            case ']': return 'ล';
            case '}': return ',';


            case 'a': return 'ฟ';
            case 'A': return 'ฤ';
            case 's': return 'ห';
            case 'S': return 'ฆ';
            case 'd': return 'ก';
            case 'D': return 'ฏ';
            case 'f': return 'ด';
            case 'F': return 'โ';
            case 'g': return 'เ';
            case 'G': return 'ฌ';
            case 'h': return '้';
            case 'H': return '็';
            case 'j': return '่';
            case 'J': return '๋';
            case 'k': return 'า';
            case 'K': return 'ษ';
            case 'l': return 'ส';
            case 'L': return 'ศ';
            case ';': return 'ว';
            case ':': return 'ซ';
            case '\'': return 'ง';
            case '\"': return '.';

            case 'z': return 'ผ';
            case 'Z': return '(';
            case 'x': return 'ป';
            case 'X': return ')';
            case 'c': return 'แ';
            case 'C': return 'ฉ';
            case 'v': return 'อ';
            case 'V': return 'ฮ';
            case 'b': return 'ิ';
            case 'B': return 'ฺ';
            case 'n': return 'ื';
            case 'N': return '์';
            case 'm': return 'ท';
            case 'M': return '?';
            case ',': return 'ม';
            case '<': return 'ฒ';
            case '.': return 'ใ';
            case '>': return 'ฬ';
            case '/': return 'ฝ';
            case '?': return 'ฦ';
            default:
                return c;
        }

    }
}
