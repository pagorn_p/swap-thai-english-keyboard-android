package com.henggon.swapthai_englishkeyboardandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

public class MainActivity extends AppCompatActivity {
    private Button decodeButton;
    private EditText encodeText;
    private EditText decodeText;
    private RadioButton engToThaiRadio;
    private RadioButton thaiToEngRadio;
    private boolean isEn;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialView();
    }

    private void initialView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        decodeButton = (Button) this.findViewById(R.id.button);
        encodeText = (EditText) this.findViewById(R.id.editText);
        decodeText = (EditText) this.findViewById(R.id.editText2);
        engToThaiRadio = (RadioButton)this.findViewById(R.id.radioButton);
        thaiToEngRadio = (RadioButton)this.findViewById(R.id.radioButton2);


       decodeText.setKeyListener(null);
        decodeText.setTextIsSelectable(true);

        decodeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isEn = engToThaiRadio.isChecked();
                String temp = encodeText.getText().toString();
                decodeText.setText(DecodeHengType(temp, isEn));
            }
        });

    }

  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
*/
    public String DecodeHengType(String hengType, boolean isEn)
    {
        String sentence = "";
        char[] tmp = hengType.toCharArray();

        for (int i = 0; i < hengType.length(); i++)
        {
            if (isEn)
            {
                sentence += CompareKey.compareKeyEngToThai(tmp[i]);
            }
            else {
                sentence += CompareKey.compareKeyThaiToEng(tmp[i]);
            }
        }

        return sentence;
    }
}
